import React from "react";
import { Form, Field, withFormik } from "formik";
import { Input, Button } from "semantic-ui-react";

interface FormHardCashVal {
    anggaranRumah: number;
    njop: number;
    luastanah: number;
    npoptk: number;
}

const placeHolders = [
    "anggaran rumah", "njop", "luas tanah", "npoptk"
];

function capitalize(theStr: string): string {
    return theStr.charAt(0).toUpperCase() + theStr.slice(1)
}

const capitalizedPlaceHolders = placeHolders.map(capitalize)



const FormHardCash: React.FC = () => (
<Form>
    <h5>Skema Hard Cash</h5>
    <Field name='anggaranRumah'>
        {({
            form: { setFieldValue }
        }: {
            form: { setFieldValue: (type: string, val: number) => void };
        }): React.ReactElement => (
            <Input
            size='mini'
            style={{ display: "flex", flex: 1 }}
            placeholder={capitalizedPlaceHolders[0]}
            id='formAnggaranrumah'
            type='number'
            name='anggaranRumah'
            labelPosition='right'
            label={{ basic: true, content: "rupiah" }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
                setFieldValue("anggaranRumah", parseInt(event.target.value, 10))
            }
            />
        )}
    </Field>
    <br />
    <Field name='njop'>
        {({
            form: { setFieldValue }
        }: {
            form: { setFieldValue: (type: string, val: number) => void };
        }): React.ReactElement => (
            <Input
            size='mini'
            style={{ display: "flex", flex: 1 }}
            id='formNjop'
            type='number'
            name='njop'
            placeholder={capitalizedPlaceHolders[1]}
            labelPosition='right'
            label={{ basic: true, content: "rupiah" }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
                setFieldValue("njop", parseInt(event.target.value, 10))
            }
            />
        )}
    </Field>
    <br />
    <Field name='luastanah'>
        {({
            form: { setFieldValue }
        }: {
            form: { setFieldValue: (type: string, val: number) => void };
        }): React.ReactElement => (
            <Input
            size='mini'
            style={{ display: "flex", flex: 1 }}
            id='formLuastanah'
            type='number'
            step='0.01'
            name='luastanah'
            placeholder={capitalizedPlaceHolders[2]}
            labelPosition= 'right'
            label={{ basic: true, content: "m2" }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
                setFieldValue("luastanah", parseFloat(event.target.value))
            }
            />
        )}
    </Field>
    <br/>
    <Field name='npoptk'>
        {({
            form: { setFieldValue }
        }: {
            form: { setFieldValue: (type: string, val: number) => void };
        }): React.ReactElement => (
            <Input
            size='mini'
            style={{ display: "flex", flex: 1 }}
            id='formNpoptk'
            type='number'
            step='0.01'
            name='npoptk'
            placeholder={capitalizedPlaceHolders[3]}
            labelPosition= 'right'
            label={{ basic: true, content: "rupiah" }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
                setFieldValue("npoptk", parseFloat(event.target.value))
            }
            />
        )}
    </Field>
    <br />
    <Button type='submit' fluid primary>
    Submit
    </Button>
</Form>
);



interface MyFormProps {
    handleSubmit: (data: FormHardCashVal) => void;
  }
  
export default withFormik<MyFormProps, FormHardCashVal>({
handleSubmit: (values: FormHardCashVal, { props }) => props.handleSubmit(values)
})(FormHardCash);