import React from "react";
import { Form, Field, withFormik } from "formik";
import { Button, Input } from "semantic-ui-react";

interface FormValues {
  payment: number;
  time: number;
  interest: number;
}

const FormKPR: React.FC = () => (
  <Form>
    <h5>Skema KPR</h5>
    <Field name='payment'>
      {({
        form: { setFieldValue }
      }: {
        form: { setFieldValue: (type: string, val: number) => void };
      }): React.ReactElement => (
        <Input
          size='mini'
          style={{ display: "flex", flex: 1 }}
          placeholder='Kemampuan Angsuran'
          id='formPayment'
          type='number'
          name='payment'
          label={{ basic: true, content: "per bulan" }}
          labelPosition='right'
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
            setFieldValue("payment", parseInt(event.target.value, 10))
          }
        />
      )}
    </Field>
    <br />
    <Field name='time'>
      {({
        form: { setFieldValue }
      }: {
        form: { setFieldValue: (type: string, val: number) => void };
      }): React.ReactElement => (
        <Input
          size='mini'
          style={{ display: "flex", flex: 1 }}
          placeholder='Waktu Cicilan'
          id='formTime'
          type='number'
          name='time'
          label={{ basic: true, content: "bulan" }}
          labelPosition='right'
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
            setFieldValue("time", parseInt(event.target.value, 10))
          }
        />
      )}
    </Field>
    <br />
    <Field name='interest'>
      {({
        form: { setFieldValue }
      }: {
        form: { setFieldValue: (type: string, val: number) => void };
      }): React.ReactElement => (
        <Input
          size='mini'
          style={{ display: "flex", flex: 1 }}
          placeholder='Bunga'
          id='formInterest'
          type='number'
          step='0.001'
          name='interest'
          label={{ basic: true, content: "% per Bulan" }}
          labelPosition='right'
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
            setFieldValue("interest", parseFloat(event.target.value) / 100)
          }
        />
      )}
    </Field>
    <br />
    <Button type='submit' fluid primary>
      Submit
    </Button>
  </Form>
);

interface MyFormProps {
  handleSubmit: (data: FormValues) => void;
}

export default withFormik<MyFormProps, FormValues>({
  handleSubmit: (values: FormValues, { props }) => props.handleSubmit(values)
})(FormKPR);
