import axios from "axios";
import { FormikValues } from "formik";

export const K_DEVELOPER_INSTALLMENT_CALCULATOR_URL =
  "https://rqxcmhhb2l.execute-api.us-east-1.amazonaws.com/default/cicilan-developer";

export const K_KPR_INSTALLMENT_CALCULATOR_URL =
  "https://mx8pyd12pk.execute-api.us-east-1.amazonaws.com/default/perhitungan-kpr";

export const K_HARD_CASH_CALCULATOR_URL =
  "https://jb2ww8i1a8.execute-api.us-east-1.amazonaws.com/default/hardcash-haskell-pemfung";

export default {
  calculate: {
    developer: async (data: FormikValues): Promise<number> => {
      const proxyurl = "https://cors-anywhere.herokuapp.com/";
      return axios
        .post(proxyurl + K_DEVELOPER_INSTALLMENT_CALCULATOR_URL, data, {
          headers: { "Access-Control-Allow-Origin": "*" }
        })
        .then(res => res.data);
    },
    kpr: async (data: FormikValues): Promise<number> => {
      const proxyurl = "https://cors-anywhere.herokuapp.com/";
      return axios
        .post(proxyurl + K_KPR_INSTALLMENT_CALCULATOR_URL, data, {
          headers: { "Access-Control-Allow-Origin": "*" }
        })
        .then(res => res.data);
    },
    cash: async (data: FormikValues): Promise<number> => {
      const proxyurl = "https://cors-anywhere.herokuapp.com/";
      return axios
        .post(proxyurl + K_HARD_CASH_CALCULATOR_URL, data, {
          headers: { "Access-Control-Allow-Origin": "*" }
        })
        .then(res => res.data);
    }
  }
};
