import React, { useCallback, useState } from "react";
import { DropdownProps, Select } from "semantic-ui-react";
import { FormikValues } from "formik";
import logo from "./logo.svg";
import "./App.css";
import K_FORM_OPTIONS from "./constants";

type FormValues = FormikValues | React.FormEvent<HTMLFormElement>;

const App: React.FC = () => {
  const [result, setResult] = useState(0);
  const [formType, setFormType] = useState<string | undefined>();

  const handleChange = (
    e: React.SyntheticEvent<HTMLElement, Event>,
    data: DropdownProps
  ): void => {
    setFormType(() => data.value as string);
  };

  const formOption = K_FORM_OPTIONS.find(option => option.key === formType);
  const submitData = useCallback(
    (values: FormValues): Promise<void> | undefined =>
      formOption && formOption.handleSubmit(values).then(setResult),
    [formOption]
  );

  const FormComponent = formOption && formOption.FormComponent;

  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <Select
          placeholder='Select Form'
          options={K_FORM_OPTIONS}
          onChange={handleChange}
          small
        />
        <br />
        {FormComponent && <FormComponent handleSubmit={submitData} />}
        <br />
        <h1>{`Rp${result.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")}`}</h1>
      </header>
    </div>
  );
};

export default App;
