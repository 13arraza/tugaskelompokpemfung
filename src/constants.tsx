import { FormikValues } from "formik";
import FormDeveloper from "./FormDeveloper";
import FormHardCash from "./FormHardCash";
import FormKPR from "./FormKPR";
import api from "./api";

type FormValues = FormikValues | React.FormEvent<HTMLFormElement>;

export default [
  {
    key: "kpr",
    value: "kpr",
    text: "Form Perhitungan KPR",
    handleSubmit: (val: FormValues): Promise<number> => api.calculate.kpr(val),
    FormComponent: FormKPR
  },
  {
    key: "developer",
    value: "developer",
    text: "Form Perhitungan Cilican Developer",
    handleSubmit: (val: FormValues): Promise<number> =>
      api.calculate.developer(val),
    FormComponent: FormDeveloper
  },
  {
    key: "cash",
    value: "cash",
    text: "Form Perhitungan Tunai Keras",
    handleSubmit: (val: FormValues): Promise<number> => api.calculate.cash(val),
    FormComponent: FormHardCash
  }
];
