import React from "react";
import { Form, Field, withFormik } from "formik";
import { Button, Input } from "semantic-ui-react";

interface FormValues {
  jumlahCicilan: number;
  tenor: number;
  totalDpPersen: number;
}

const FormDeveloper: React.FC = () => (
  <Form>
    <Field name='jumlahCicilan'>
      {({
        form: { setFieldValue }
      }: {
        form: { setFieldValue: (type: string, val: number) => void };
      }): React.ReactElement => (
        <Input
          size='mini'
          style={{ display: "flex", flex: 1 }}
          placeholder='Kemempuan angsuran'
          id='formJumlahCicilan'
          type='number'
          name='jumlahCicilan'
          label={{ basic: true, content: "per bulan" }}
          labelPosition='right'
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
            setFieldValue("jumlahCicilan", parseInt(event.target.value, 10))
          }
        />
      )}
    </Field>
    <br />
    <Field name='tenor'>
      {({
        form: { setFieldValue }
      }: {
        form: { setFieldValue: (type: string, val: number) => void };
      }): React.ReactElement => (
        <Input
          size='mini'
          style={{ display: "flex", flex: 1 }}
          id='formTenor'
          type='number'
          name='tenor'
          placeholder='Lama pinjaman'
          label={{ basic: true, content: "tahun" }}
          labelPosition='right'
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
            setFieldValue("tenor", parseInt(event.target.value, 10))
          }
        />
      )}
    </Field>
    <br />
    <Field name='totalDpPersen'>
      {({
        form: { setFieldValue }
      }: {
        form: { setFieldValue: (type: string, val: number) => void };
      }): React.ReactElement => (
        <Input
          size='mini'
          style={{ display: "flex", flex: 1 }}
          id='formTotalDpPersen'
          type='number'
          step='0.01'
          name='totalDpPersen'
          placeholder='Uang muka (DP)'
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void =>
            setFieldValue("totalDpPersen", parseFloat(event.target.value))
          }
        />
      )}
    </Field>
    <br />
    <Button type='submit' fluid primary>
      Submit
    </Button>
  </Form>
);

interface MyFormProps {
  handleSubmit: (data: FormValues) => void;
}

export default withFormik<MyFormProps, FormValues>({
  handleSubmit: (values: FormValues, { props }) => props.handleSubmit(values)
})(FormDeveloper);
