# Kredit Pemilikan Rumah (KPR)

KPR merupakan salah satu metode pembelian rumah dengan cara kredit yang umumnya
melalui bank penyedia KPR. Karena pembayaran bersifat kredit, pembeli akan
melakukan pembayaran secara berangsur-angsur selama periode yang ditentukan
dengan tambahan bunga yang ditetapkan oleh bank penyedia KPR.

Pada umumnya, perhitungan digunakan rumus anuitas yang akan menghitung
total pembayaran (termasuk pajak sesuai rentang waktu kredit) yang perlu
dilakukan dan membaginya secara merata untuk tiap bulannya.

```
a = (P * i) / (1 - ((1 + i) ^ (-t)))
```

> a : amount (cicilan tiap bulannya)
>
> P : Price (harga rumah)
>
> i : interest (bunga bulanan)
>
> t : time (retang waktu kredit)

## Contoh Perhitungan

Harga rumah : Rp 12.000.000,-

Periode : 12 bulan

Bunga Tahunan : 10%

Bunga Bulanan : 0.83%

```
a = (P * i) / (1 - ((1 + i) ^ t))
  = (12000000 * 0.83%) / (1 - ((1 + 0.83%) ^ (-12)))
  = (100000) / (1 - ((1.0083) ^ (-12)))
  = (100000) / (1 - 0.9052)
  = (100000) / (0.0947)
  = 1054991
```

Sehingga kita mendapatkan cicilan per bulan sebesar Rp 1.054.991.

Dari perhitungan di atas kita dapat mendapatkan gambaran detail angsuran pada
tiap bulannya:

| Bulan  	| Sisa Hutang 	| Bayar Pokok 	| Bayar Bunga 	| Jumlah Angsuran 	| Sisa Pokok 	|
|--------	|-------------	|-------------	|-------------	|-----------------	|------------	|
| 1      	| 12.000.000  	| 954.991     	| 100.000     	| 1.054.991       	| 11.045.009 	|
| 2      	| 11.045.009  	| 962.949     	| 92.042      	| 1.054.991       	| 10.082.060 	|
| 3      	| 10.082.060  	| 970.973     	| 84.017      	| 1.054.991       	| 9.111.087  	|
| 4      	| 9.111.087   	| 979.065     	| 75.926      	| 1.054.991       	| 8.132.022  	|
| 5      	| 8.132.022   	| 987.224     	| 67.767      	| 1.054.991       	| 7.144.798  	|
| 6      	| 7.144.798   	| 995.451     	| 59.540      	| 1.054.991       	| 6.149.348  	|
| 7      	| 6.149.348   	| 1.003.746   	| 51.245      	| 1.054.991       	| 5.145.602  	|
| 8      	| 5.145.602   	| 1.012.111   	| 42.880      	| 1.054.991       	| 4.133.491  	|
| 9      	| 4.133.491   	| 1.020.545   	| 34.446      	| 1.054.991       	| 3.112.946  	|
| 10     	| 3.112.946   	| 1.029.049   	| 25.941      	| 1.054.991       	| 2.083.897  	|
| 11     	| 2.083.897   	| 1.037.625   	| 17.366      	| 1.054.991       	| 1.046.272  	|
| 12     	| 1.046.272   	| 1.046.272   	| 8.719       	| 1.054.991       	| 0          	|
|       	| Jumlah        | 12.000.000  	| 659.888     	| 12.659.888      	|            	|

Pada tabel di atas dapat kita lihat bahwa jumlah angsuran tetap untuk tiap
bulannya, sebagai hasil pembagian total harga rumah ditambah dengan total
bunga dari sisa hutang untuk tiap bulannya.

Sebagai contoh, pada bulan pertama pembeli memiliki hutang sebesar `Rp 12.000.000`
kepada bank penyedia KPR. Dengan bunga bulanan 0.83% maka pembeli diwajibkan
untuk membayar bunga sebesar `0.83% x Rp 12.000.000` yakni `Rp 100.000`. Agar jumlah
angsuran sesuai dengan formula sebelumnya, maka pembeli akan membayar hutang
sebesar `Rp 954.991` sehingga sisa hutang pokok adalah `Rp 11.045.009`.

Pada bulan ke-7, sisa hutang sebesar `Rp 6.149.348`, sehingga wajib membayar bunga
sebesar `0.83% x Rp 6.149.348` yakni `Rp 51.245`. Dengan jumlah angsuran konstan
(sebesar `Rp 1.054.991`), maka pembeli dapat mengangsur hutang pokok sebesar
`Rp 1.003.746` sehingga sisa hutang adalah `Rp 5.145.602`.
